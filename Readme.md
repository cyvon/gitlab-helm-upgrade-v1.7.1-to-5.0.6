# Getting Started
## Prerequisites (or versions I used)
- Decent computer to do the upgrade on
- Decently fast internet connection (you'll be downloading alot from google.)
- helm v3.5.0
- minikube v1.17.1
- I use the ce version of gitlab.... everything should be the same for the default ee version
## Good Ref Material
- https://docs.gitlab.com/charts/installation/upgrade.html
- https://docs.gitlab.com/charts/installation/version_mappings.html
- https://docs.gitlab.com/charts/installation/database_upgrade.html
- https://docs.gitlab.com/charts/backup-restore/backup.html

## First Steps
Start by adding the helm repo and setting up a minikube Cluster

```
helm repo add gitlab https://charts.gitlab.io/

minikube start --cpus 7 --memory 24240 --kubernetes-version=1.15.0
minikube addons enable ingress
minikube dashboard --url &
```

## Get your Data in minikube

- START with the version that you are currently on!
- You will want to backup and restore your current data. The basics of this are:

**On Current Production**
```
kubectl get pods -lrelease=gitlab,app=task-runner
kubectl exec <Task Runner pod name> -it -- backup-utility
```
- Get the minio url and secret from your curret config and log in
- Look in the backups bin and get a link for your most current backup

```
kubectl get secrets | grep rails-secret
kubectl get secrets <rails-secret-name> -o jsonpath="{.data['secrets\.yml']}" | base64 --decode > secrets.yaml
```
**On Local/Minikube**
```
kubectl get secrets | grep rails-secret
kubectl delete secret <rails-secret-name>
kubectl create secret generic <rails-secret-name> --from-file=secrets.yml=secrets.yaml

kubectl get pods -lrelease=gitlab,app=task-runner
kubectl exec <Task Runner pod name> -it -- backup-utility --restore -f <URL TO BACKUP TAR FILE>

kubectl delete pods -lapp=sidekiq,release=gitlab
kubectl delete pods -lapp=unicorn,release=gitlab
kubectl delete pods -lapp=webservice,release=gitlab
```

## Start the Upgrade Procedures
- As you go, at each upgrade watch the kube dashboard and wait until you are "all green" before moving forward.
- You "should" check the version to ensure it upgraded properly, and look around to ensure projects are working.


**1.7.1    - 11.9.1**

`helm install gitlab gitlab/gitlab -f values-minikube.yaml -f gitlab.yaml --version 1.7.1`

**1.9.8    - 11.11.8**

`helm upgrade gitlab gitlab/gitlab -f values-minikube.yaml -f gitlab.yaml --version 1.9.8`

**2.3.7    - 12.3.5**

`helm upgrade gitlab gitlab/gitlab -f values-minikube.yaml -f gitlab.yaml --version 2.3.7`


**custom-2.6.9 - 12.6.8**
---
There is an error in the chart.... the custom chart cherrypicks this update:

https://gitlab.com/gitlab-org/charts/gitlab/-/merge_requests/1205/diffs
---
`helm upgrade gitlab gitlab-2.6.9 -f values-minikube.yaml -f gitlab.yaml`

**custom-3.0.0 - 12.7.0**
---
### v3.0.0 manual steps:
https://docs.gitlab.com/charts/installation/upgrade.html#upgrade-steps-for-30-release
```
./database-upgrade-3.0.0.sh pre
kubectl get secret gitlab-postgresql-password -o yaml > postgresql-password.backup.yaml
kubectl delete secret gitlab-postgresql-password
kubectl delete services -lrelease=gitlab
```

Same reason/error/cherrypick for custom as above

`helm upgrade gitlab gitlab-3.0.0 -f values-minikube.yaml -f gitlab.yaml --set gitlab.migrations.enabled=false`

Wait on rollout

```
kubectl rollout status -w deployment/gitlab-task-runner
./database-upgrade-3.0.0.sh post
```
---

**3.3.9    - 12.10.10**

- Note, you may not need to do this version... I did, but it may not be needed.

`helm upgrade gitlab gitlab/gitlab -f values-minikube.yaml -f gitlab.yaml --version 3.3.9`

**3.3.12   - 12.10.13**

`helm upgrade gitlab gitlab/gitlab -f values-minikube.yaml -f gitlab.yaml --version 3.3.12`

---
**3.3.13   -**

~~`helm upgrade gitlab gitlab/gitlab -f values-minikube.yaml -f gitlab.yaml --version 3.3.13`~~

This version seems to have postgresql Version mismatch errors, so skip it.

---

**4.0.0    - 13.0.0**
---
### v4.0.0 manual steps:
https://docs.gitlab.com/charts/installation/upgrade.html#upgrade-steps-for-40-release

```
./database-upgrade-4.0.0.sh pre
kubectl delete statefulset gitlab-postgresql
kubectl delete pvc data-gitlab-postgresql-0

helm upgrade gitlab gitlab/gitlab -f values-minikube.yaml -f gitlab.yaml --version 4.0.0 --set gitlab.migrations.enabled=false
```

Wait on rollout

```
kubectl rollout status -w deployment/gitlab-task-runner
./database-upgrade-4.0.0.sh post
```
---

**4.8.3    - 13.8.3**

- Note, you may not need to do this version... I did, but it may not be needed.

`helm upgrade gitlab gitlab/gitlab -f values-minikube.yaml -f gitlab.yaml --version 4.8.3`

**4.12.8    - 13.12.8**

`helm upgrade gitlab gitlab/gitlab -f values-minikube.yaml -f gitlab.yaml --version 4.12.8`

**5.0.0    - 14.0.0**
---
### v5.0.0 manual steps: 
https://docs.gitlab.com/charts/installation/upgrade.html#upgrade-steps-for-50-release
```
./database-upgrade-5.0.0.sh pre
kubectl delete statefulset gitlab-postgresql
kubectl delete pvc data-gitlab-postgresql-0

helm upgrade gitlab gitlab/gitlab -f values-minikube.yaml -f gitlab.yaml --version 5.0.0 --set gitlab.migrations.enabled=false
```

Wait on rollout

```
kubectl rollout status -w deployment/gitlab-task-runner
./database-upgrade-5.0.0.sh post
```
---

**5.0.6    - 14.0.6**

`helm upgrade gitlab gitlab/gitlab -f values-minikube.yaml -f gitlab.yaml --version 5.0.6`

# Final Steps
OK, you are on the most current version on your Minikube! Congrats!

Make sure everything works! Then:
- (The scary part) Delete your old install with helm delete
    If you can't do that due to old helm versions/etc... then delete the entire namespace and then the gitlab ClusterRoles and ClusterRoleBindings
- Do a clean helm install of gitlab at the latest version (that would match your minikube version)
- Wait for everything to come up. If you are overriding values, you'll most likly need to update some stuff... locations of settings have changed a little bit on the new charts. Feel free to helm delete and helm install until a clean copy is up and running properly
- Backup/Restore from minikube to your prod environment just like you started out with (just the oppisite direction now.)
- Enjoy all the new features!

Brought to you by Eric Schultz

